fibonacciList x
	| x <= 0	= [0]
	| x == 1	= [1]
	| x == 2	= [2, 1]
	| otherwise = (head fLast + head (tail fLast)) : fLast
	where
		fLast = fibonacciList (x - 1)

limitedFibonacciList l = head [ fibonacciList (x - 1) | x <- [1..], head (fibonacciList x) > l ]

sumEven l = sum $ filter (\n -> mod n 2 == 0) (limitedFibonacciList l)