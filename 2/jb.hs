fib2 x y = x+y : fib2 y (x+y)   -- Generates the Fibonacci series starting with the two provided terms
fib = fib2 1 2                  -- Generates the entire Fibonacci series

main = print $ sum [x | x <- takeWhile (<=4000000) fib, (x `mod` 2) == 0]
