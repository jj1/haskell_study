evenFibSum n = evenFibSumHelper n 1 1

evenFibSumHelper n parent grandparent | fib > n   = 0
                                      | even fib  = fib + (evenFibSumHelper n fib parent)
                                      | otherwise = evenFibSumHelper n fib parent
                                       where fib = parent + grandparent

main = print $ evenFibSum 4000000
