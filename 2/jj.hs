module Main where

import System.Environment (getArgs)

fib :: [Integer]
fib = 1:2:fib'
    where fib' = zipWith (+) fib $ tail fib

evenFibsUpto :: Integer -> Integer
evenFibsUpto n = sum $ filter even $ takeWhile (<n) fib

main :: IO ()
main = do
        args <- getArgs
        print $ evenFibsUpto $ if length args > 0 then read $ head args else 4000000