module Main where

-- This is an infinite list, no biggie, just don't try to get the infinite'th member
fibonacciSeq :: [Integer]
fibonacciSeq = map memoisedFibonacci [0..]
    where memoisedFibonacci :: Int -> Integer
          memoisedFibonacci = (!!) (map (fibonacci memoisedFibonacci) [0 ..])
          -- first parameter allows injecting memoization
          fibonacci :: (Int -> Integer) -> Int -> Integer
          fibonacci _ 0 = 1
          fibonacci _ 1 = 2
          fibonacci f n = f (n-1) + f (n-2)

main::IO ()
main = print $ sum [i | i <- takeWhile (<=4000000) fibonacciSeq, even i]
-- displays '4613732'
