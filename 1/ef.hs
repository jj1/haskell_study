module Main where

main::IO()
main = print $ sum [ c | c <- [1..999] :: [Int] , (c `mod` 3 == 0) || (c `mod` 5 == 0)]