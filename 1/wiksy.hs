sumfac fa n
	| n < fa			= 0
	| remain == 0		= sumfac fa (n - fa) + n
	| otherwise			= sumfac fa (n - remain)
		where
		remain = mod n fa

sumfac15 n = sumfac 3 n + sumfac 5 n - sumfac 15 n
