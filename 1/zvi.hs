main = let multiplesOfThree = map (*3) [1 .. 999 / 3]
           multiplesOfFive = map (*5) [1 .. 999 / 5]
           multiplesOfNegativeFifteen = map (*(-15)) [1 .. 999 / 15]
           result = sum (multiplesOfThree ++ multiplesOfFive ++ multiplesOfNegativeFifteen)
       in print result